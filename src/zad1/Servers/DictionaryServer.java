package zad1.Servers;

import zad1.Tools;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

public class DictionaryServer extends Thread {

    public HashMap<String, String> Dictionary;

    private ServerSocket server;
    private int port;
    private String languageCodeFrom;
    private String languageCodeTo;

    public DictionaryServer(String languageCodeFrom, String languageCodeTo, HashMap<String, String> dictionary) {
        this.languageCodeFrom = languageCodeFrom;
        this.languageCodeTo = languageCodeTo;
        this.port = port;
        try {
            if (languageCodeFrom.length() != 3 || languageCodeTo.length() != 3) {
                throw new Exception("Language codes must be provided as ISO-3166 code!");
            }
            this.languageCodeFrom = languageCodeFrom.toUpperCase();
            this.languageCodeTo = languageCodeTo.toUpperCase();
            Dictionary = dictionary;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String GetLanguageCodefrom()
    {
        return languageCodeFrom;
    }
    public String GetLanguageCodeTo()
    {
        return languageCodeTo;
    }

    private void SendWelcomeMessage(int i) {
        try {
            InetAddress host = InetAddress.getLocalHost();
            Socket socket;
            ObjectOutputStream oos;
            socket = new Socket(host.getHostName(), 9876);
            oos = new ObjectOutputStream(socket.getOutputStream());
            System.out.println("Sending hello message to main server from dictionary server: " + languageCodeFrom + "-" + languageCodeTo);
            oos.writeObject("ND:" + languageCodeFrom + "-" + languageCodeTo + "," + i);
            oos.flush();
            Thread.sleep(100);
        } catch (InterruptedException | IOException e) {
            e.printStackTrace();
        }
    }

    public void run() {
        try {
            server = new ServerSocket(0);
            SendWelcomeMessage(server.getLocalPort());

            while (true) {
                Socket socket = server.accept();
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                String message = (String) ois.readObject();
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());

                if (Tools.IsPatternMatchedInWord("[T][W][:][\\w+]*", message))
                {
                    String newMessage = message.replace("TW:", "");

                    if (Dictionary.containsKey(newMessage))
                    {
                        oos.writeObject("TW:" + Dictionary.get(newMessage));
                    }
                    else
                    {
                        oos.writeObject("TW:" + "-no translation-");
                    }
                    oos.flush();
                }
                oos.close();
                ois.close();
                socket.close();
                if (message.equalsIgnoreCase("exit")) break;
            }
            System.out.println("Shutting down Socket server!");
            server.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
