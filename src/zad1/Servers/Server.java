package zad1.Servers;

import jdk.nashorn.internal.runtime.regexp.joni.Regex;
import zad1.Tools;

import java.io.Console;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Server extends Thread {

    private HashMap<String, Integer> hashMap = new HashMap<>();
    private ServerSocket server;
    private int port = 9876;

    public ArrayList<String> getAllCodesFrom()
    {
        ArrayList<String> codesFrom = new ArrayList<>();
        hashMap.forEach((x,y) -> {
            String splitted = x.split("-")[0];
            if(!codesFrom.contains(splitted))
            {
                codesFrom.add(splitted);
            }
        });
        return codesFrom;
    }
    public ArrayList<String> getAllCodesTo()
    {
        ArrayList<String> codesFrom = new ArrayList<>();
        hashMap.forEach((x,y) -> {
            String splitted = x.split("-")[1];
            if(!codesFrom.contains(splitted))
            {
                codesFrom.add(splitted);
            }
        });
        return codesFrom;
    }
    public void run() {
        try {
            InetAddress host = InetAddress.getLocalHost();
            server = new ServerSocket(port);

            while (true) {
                Socket socket = server.accept();
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                String message = (String) ois.readObject();

                if (Tools.IsPatternMatchedInWord("[N][D][:][\\w\\W]{3}[-][\\w\\W]{3}[,][0-9]{4,5}", message))
                {
                    String newMessage = message.replace("ND:", "");
                    String[] splitted = newMessage.split(",");

                    String countryCode = splitted[0];
                    Integer portNumber = Integer.parseInt(splitted[1]);
                    hashMap.put(countryCode, portNumber);
                    System.out.println("HashMap length: " + hashMap.size() );
                }
                else if (Tools.IsPatternMatchedInWord("[T][W][:][\\w+\\W+]{3}[-][\\w+\\W+]{3}[,][\\w+\\w+]*", message))
                {
                    try {

                        String newMessage = message.replace("TW:", "");
                        String language =  newMessage.split(",")[0];
                        String wordToTranslate =  newMessage.split(",")[1];
                        Integer portDictionary = hashMap.get(language);

                        if(portDictionary == null)
                        {
                            oos.writeObject("-Such dictionary dosen't exists!-");
                            oos.flush();
                            continue;
                        }
                        Socket socketDictionary = new Socket(host.getHostName(), portDictionary);
                        ObjectOutputStream oosDictionary = new ObjectOutputStream(socketDictionary .getOutputStream());
                        oosDictionary.writeObject("TW:" + wordToTranslate);
                        oosDictionary.flush();
                        ObjectInputStream oisDictinary = new ObjectInputStream(socketDictionary .getInputStream());
                        newMessage = (String) oisDictinary.readObject();
                        System.out.println("Translated word: " + newMessage);
                        oos.writeObject(newMessage);
                        oos.flush();
                        Thread.sleep(100);
                        oisDictinary.close();
                        oosDictionary.close();
                        socketDictionary.close();

                    } catch (InterruptedException | IOException e) {
                        e.printStackTrace();
                    }
                    ois.close();
                    oos.close();
                    socket.close();
                }
                if (message.equalsIgnoreCase("exit")) break;
            }
            System.out.println("Shutting down Socket server!");
            server.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
