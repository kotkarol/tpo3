package zad1.Servers;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class ServerClient {

    public static String run(String codeFrom, String codeTo, String wordToTranslate) {
        String message = "";
        try {
            InetAddress host = InetAddress.getLocalHost();

            Socket socket = new Socket(host.getHostName(), 9876);
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            oos.writeObject("TW:"+codeFrom+"-"+codeTo+","+ wordToTranslate);
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            message = (String) ois.readObject();

            ois.close();
            oos.close();
            socket.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return message;
    }
}
