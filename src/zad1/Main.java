package zad1;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableIntegerArray;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import zad1.Servers.DictionaryServer;
import zad1.Servers.Server;
import zad1.Servers.ServerClient;

import java.util.HashMap;

public class Main extends Application {

    static Server server;
    Button button;

    public static void main(String[] args) throws InterruptedException {
        server = new Server();
        HashMap<String, String> polAngDic = new HashMap<>();
        HashMap<String, String> polFraDic= new HashMap<>();
        HashMap<String, String> fraJpaDic= new HashMap<>();
        polAngDic.put("jablko", "apple");
        polAngDic.put("pies", "dog");
        polAngDic.put("kot", "cat");
        polFraDic.put("pies", "chien");
        polFraDic.put("kot", "char");
        polFraDic.put("jablko", "pomme");

        Thread t = new Thread(server);
        t.start();

        DictionaryServer dictionaryServerPolEng = new DictionaryServer("POL", "ENG", polAngDic);
        DictionaryServer dictionaryServerPolFra = new DictionaryServer("POL", "FRA", polFraDic);
        DictionaryServer dictionaryServerFraJpa = new DictionaryServer("FRA", "JPA", fraJpaDic);
        Thread dicServerThread = new Thread(dictionaryServerPolEng);
        Thread dicServerFraThread = new Thread(dictionaryServerPolFra);
        Thread dicServerFraJpaThread = new Thread(dictionaryServerFraJpa);
        dicServerThread.start();
        dicServerFraThread.start();
        dicServerFraJpaThread.start();

        Thread.sleep(1000);
        launch(args);

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("TPO 3 Servers");

        button = new Button();
        button.setText("Translate");

        ObservableList<String> itemsFrom = FXCollections.observableArrayList();
        itemsFrom.addAll(server.getAllCodesFrom());

        ObservableList<String> itemsTo = FXCollections.observableArrayList();
        itemsTo.addAll(server.getAllCodesTo());

        GridPane layout = new GridPane();
        layout.setPadding(new Insets(20, 20, 20, 20));

        layout.setHgap(10);
        layout.setVgap(10);

        ComboBox<String> codeFrom = new ComboBox<>();
        ComboBox<String> codeTo = new ComboBox<>();
        TextField textAreaToTranslate = new TextField("Write word to translate");
        TextField textAreaTranslated = new TextField("Translated word");

        textAreaTranslated.setDisable(true);

        codeFrom.setItems(itemsFrom);
        codeTo.setItems(itemsTo);
        codeFrom.getSelectionModel().selectFirst();
        codeTo.getSelectionModel().selectFirst();

        layout.add(codeFrom , 0, 0);
        layout.add(codeTo, 3 , 0);
        layout.add(button , 3 ,4);
        layout.add(textAreaToTranslate, 0 ,2 , 3 ,1);
        layout.add(textAreaTranslated,0,3 , 3, 1);


        button.setOnAction(e -> {
            String translated = ServerClient.run(codeFrom.getValue(), codeTo.getValue(), textAreaToTranslate.getText());
            textAreaTranslated.setText(translated.replace("TW:" , ""));
        });

        Scene scene = new Scene(layout, 300, 250);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
