package zad1;

import java.util.ArrayList;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Tools {
    public static <T> T getFirstOrDefault(ArrayList<T> list, Predicate<T> predicate)
    {
        for(T element: list)
        {
            boolean testResult = predicate.test(element);
            if (testResult) return element;
        }
        return null;
    }
    public static boolean IsPatternMatchedInWord(String patternRegex, String word)
    {
        Pattern pattern = Pattern.compile(patternRegex);
        Matcher matcher = pattern.matcher(word);
        return  matcher.matches();
    }
}
